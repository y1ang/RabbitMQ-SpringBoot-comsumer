# RabbitMQ-SpringBoot-comsumer

#### 项目介绍
RabbitMQ消息中间件极速入门与实战

#### RabbitMQ安装

1. 环境准备
- yum install build-essential openssl openssl-devel unixODBC unixODBC-devel make gcc gcc-c++ kernel-devel m4 ncurses-devel tk tc xz
- yum -y install wget
2. 下载安装包
- wget http://download.oracle.com/otn-pub/java/jdk/8u151-b12/e758a0de34e24606bca991d704f6dcbf/jdk-8u151-linux-x64.tar.gz?AuthParam=1511877655_249331a15c5f013ed07d656d98e82d72
- wget http://www.rabbitmq.com/releases/erlang/erlang-18.3-1.el7.centos.x86_64.rpm
- wget http://repo.iotti.biz/CentOS/7/x86_64/socat-1.7.3.2-5.el7.lux.x86_64.rpm<br/>
- wget http://www.rabbitmq.com/releases/rabbitmq-server/v3.6.5/rabbitmq-server-3.6.5-1.noarch.rpm

注： 
- 修改root密码：sudo passwd root
- 切换到root用户：su root
- 解压jdk: tar -zxvf jdk-8u151-linux-x64.tar.gz
- 安装jdk:
<pre>
    - vi /etc/profile
    - #在该文件中导入环境变量，其中HOME为jdk解压根目录
    - export JAVA_HOME=/usr/local/java/jdk1.8.0_151
    - export PATH=$JAVA_HOME/bin:$PATH
    - export CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
</pre>

#### 配置主机名和ip绑定

1. vi /etc/hostname  //配置主机名
2. vi /etc/hosts     //配置主机名与ip绑定

#### 安装RabbitMQ

1. rpm -ivh erlang-18.3-1.el7.centos.x86_64.rpm
2. rpm -ivh socat-1.7.3.2-5.el7.lux.x86_64.rpm
3. rpm -ivh rabbitmq-server-3.6.5-1.noarch.rpm

#### 修改RabbitMQ配置文件

1. vi /usr/lib/rabbitmq/lib/rabbitmq_server-3.6.5/ebin/rabbit.app

#### 启动/停止RabbitMQ

1. service rabbitmq-server start    //启动
2. service rabbitmq-server stop    //停止

#### 启动管控UI

1. rabbitmq-plugins enable rabbitmq_management

#### AMQP核心概念
1. Channel: 网络信道，几乎所有的操作都在Channel中进行，Channel是进行消息读写的通道。客户可建立多个Channel,每个Channel代表一个回话任务。
2. Message: 消息，服务器和应用程序之间传送的数据，由Properties和Body组成。Properties可以对消息进行修饰，比如消息的优先级、延迟等高级特性；Body则是消息提内容。
3. Virtual host: 虚拟地址，用于进行逻辑隔离，最上层的消息路由。一个Virtual Host里面可以有若干个Exchange和Queue，同一个Virtual Host里面不能有相同名称的Exchange或Queue。
4. Exchange：交换机，接收消息，根据路由键转发消息到绑定的队列。
5. Binding：Exchange和Queue之间的虚拟连接，binding中可以包含routing key。
6. Routing Key：一个路由规则，虚拟机可用他来确定如何路由一个特定消息。
7. Queue：也称为Message Queue，消息队列，保存消息并将他们转发给消费者。

![输入图片说明](https://images.gitee.com/uploads/images/2018/0906/153159_3f86641e_1629815.png "rabbitmq.png")