package xyz.pascall.rabbitmq.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import xyz.pascall.rabbitmq.pojo.Message;

public interface MessageRepository extends JpaRepository<Message, String> {
}
