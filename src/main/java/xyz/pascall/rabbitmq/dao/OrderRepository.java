package xyz.pascall.rabbitmq.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import xyz.pascall.rabbitmq.pojo.Order;

public interface OrderRepository extends JpaRepository<Order, String> {
}
