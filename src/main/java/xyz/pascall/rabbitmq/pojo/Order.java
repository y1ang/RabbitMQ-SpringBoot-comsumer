package xyz.pascall.rabbitmq.pojo;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "t_order")
public class Order implements Serializable {

    private static final long serialVersionUID = 358446696354889216L;

    @Id // 主键
    @Column(name = "id")
    private String id;
    @Column(name = "name") // 映射为字段，值不能为空
    private String name;
    @Column(name = "message_id")
    private String messageId;

    public Order(String id, String name, String messageId) {
        this.id = id;
        this.name = name;
        this.messageId = messageId;
    }

    public Order() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }
}
